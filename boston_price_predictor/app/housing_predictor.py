
import pandas as pd
from sklearn import datasets
import sklearn
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score


# In[98]:


print(sklearn.__version__)


# In[99]:


bh = datasets.load_boston()


# In[100]:


X = bh.data
y = bh.target


# In[101]:


X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42)
print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)


# In[102]:


nodes = 100


# In[103]:


rfr = RandomForestRegressor(nodes)


# In[104]:


rfr.fit(X_train, y_train)


# In[105]:


y_pred = rfr.predict(X_test)


# In[106]:


r2_score(y_test, y_pred)
